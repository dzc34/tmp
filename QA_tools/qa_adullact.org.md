# QA

https://adullact.org

## URL
* [http  + with www](http://www.adullact.org)
* [http  + without www](http://adullact.org)
* [https + with www](https://www.adullact.org)
* [https + without www](https://adullact.org)

### Files

* [/humans.txt](https://adullact.org/humans.txt)
* [/robots.txt](https://adullact.org/robots.txt)
* [/.well-known/security.txt](https://adullact.org/.well-known/security.txt)

## QA - Online tools
`*` preconfigured tools


* Security 
    * [Hardenize](https://www.hardenize.com) (DNS, SMTP, web server)
    * [Mozilla Observatory](https://observatory.mozilla.org/analyze/adullact.org) `*` (HTTP header, SSL, cookies, ...)
    * [Security Headers](https://securityheaders.io/?q=https://adullact.org) `*` (HTTP header)
    * Content-Security-Policy (CSP)
        * [cspvalidator.org](https://cspvalidator.org/#url=https://adullact.org) `*` 
        * [csp-evaluator.withgoogle.com](https://csp-evaluator.withgoogle.com/?csp=https://adullact.org) `*` 
    * SSL
        * [ssllabs.com](https://www.ssllabs.com/ssltest/analyze?d=adullact.org) `*` 
        * [tls.imirhil.fr](https://tls.imirhil.fr/https/adullact.org) `*` 
    * DNS
        * [DNSViz](http://dnsviz.net/d/adullact.org/dnssec/) `*` (DNSSEC)
        * [DNSSEC Analyzer (Verisign Labs)](https://dnssec-debugger.verisignlabs.com/adullact.org) `*`   (DNSSEC)
        * [Zonemaster (iiS and AFNIC)](https://zonemaster.net/domain_check)
* W3C tools
    * [HTML validator](https://validator.w3.org/nu/?doc=https://adullact.org&showsource=yes&showoutline=yes&showimagereport=yes) `*` 
    * [CSS validator](https://jigsaw.w3.org/css-validator/validator?uri=https://adullact.org&profile=css3) `*` 
    * [i18n checker](https://validator.w3.org/i18n-checker/check?uri=https://adullact.org) `*` 
    * [Link checker](https://validator.w3.org/checklink?uri=https://adullact.org&hide_type=all&depth=&check=Check) `*` 
* Web accessibility
    * [Asqatasun](https://app.asqatasun.org)
* Web perf
    * [Yellowlab](http://yellowlab.tools)
    * [Webpagetest](https://www.webpagetest.org/)
    * [Test a single asset from 14 locations](https://tools.keycdn.com/performance?url=https://adullact.org) `*`
* HTTP/2
    * [Http2.pro](https://http2.pro/check?url=https://adullact.org) `*` (check server HTTP/2, ALPN, and Server-push support)
* Global tools (webperf, accessibility, security, ...)
    * [Dareboost](https://www.dareboost.com)  (free trial)
    * [Sonarwhal](https://sonarwhal.com/scanner/)

* Social networks
  * [Twitter card validator](https://cards-dev.twitter.com/validator)
* structured data (JSON-LD, rdf, schema.org, microformats.org, ...)
  * [Google structured data testing tool](https://search.google.com/structured-data/testing-tool#url=https://adullact.org/)  `*` 
  * [Yandex structured data testing tool](https://webmaster.yandex.com/tools/microtest/)
  * [Structured Data Linter](http://linter.structured-data.org/?url=https://adullact.org)  `*` 
  * [Microdata Parser](http://tools.seomoves.org/microdata/)
* Google image
  * [images used on the website](https://www.google.fr/search?tbm=isch&q=site:adullact.org)  `*`  (site:adullact.org)
  * [images used on the website but hosted on other domains](https://www.google.fr/search?tbm=isch&q=site:adullact.org+-src:adullact.org) `*`  (site:adullact.org -src:adullact.org)
  * [images hosted on the domain name](https://www.google.fr/search?tbm=isch&q=src:adullact.org)  `*`    (src:adullact.org)
  * [images hosted on the domain name and used by other domain names (hotlinks)](https://www.google.fr/search?tbm=isch&q=src:adullact.org+-site:adullact.org)  `*`   (src:adullact.org -site:adullact.org)



