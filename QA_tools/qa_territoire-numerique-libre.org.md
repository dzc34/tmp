# QA

https://territoire-numerique-libre.org

## URL
* [http  + with www](http://www.territoire-numerique-libre.org)
* [http  + without www](http://territoire-numerique-libre.org)
* [https + with www](https://www.territoire-numerique-libre.org)
* [https + without www](https://territoire-numerique-libre.org)

### Files

* [/humans.txt](https://territoire-numerique-libre.org/humans.txt)
* [/robots.txt](https://territoire-numerique-libre.org/robots.txt)
* [/.well-known/security.txt](https://territoire-numerique-libre.org/.well-known/security.txt)

## QA - Online tools
`*` preconfigured tools


* Security 
    * [Hardenize](https://www.hardenize.com) (DNS, SMTP, web server)
    * [Mozilla Observatory](https://observatory.mozilla.org/analyze/territoire-numerique-libre.org) `*` (HTTP header, SSL, cookies, ...)
    * [Security Headers](https://securityheaders.io/?q=https://territoire-numerique-libre.org) `*` (HTTP header)
    * Content-Security-Policy (CSP)
        * [cspvalidator.org](https://cspvalidator.org/#url=https://territoire-numerique-libre.org) `*` 
        * [csp-evaluator.withgoogle.com](https://csp-evaluator.withgoogle.com/?csp=https://territoire-numerique-libre.org) `*` 
    * SSL
        * [ssllabs.com](https://www.ssllabs.com/ssltest/analyze?d=territoire-numerique-libre.org) `*` 
        * [tls.imirhil.fr](https://tls.imirhil.fr/https/territoire-numerique-libre.org) `*` 
    * DNS
        * [DNSViz](http://dnsviz.net/d/territoire-numerique-libre.org/dnssec/) `*` (DNSSEC)
        * [DNSSEC Analyzer (Verisign Labs)](https://dnssec-debugger.verisignlabs.com/territoire-numerique-libre.org) `*`   (DNSSEC)
        * [Zonemaster (iiS and AFNIC)](https://zonemaster.net/domain_check)
* W3C tools
    * [HTML validator](https://validator.w3.org/nu/?doc=https://territoire-numerique-libre.org&showsource=yes&showoutline=yes&showimagereport=yes) `*` 
    * [CSS validator](https://jigsaw.w3.org/css-validator/validator?uri=https://territoire-numerique-libre.org&profile=css3) `*` 
    * [i18n checker](https://validator.w3.org/i18n-checker/check?uri=https://territoire-numerique-libre.org) `*` 
    * [Link checker](https://validator.w3.org/checklink?uri=https://territoire-numerique-libre.org&hide_type=all&depth=&check=Check) `*` 
* Web accessibility
    * [Asqatasun](https://app.asqatasun.org)
* Web perf
    * [Yellowlab](http://yellowlab.tools)
    * [Webpagetest](https://www.webpagetest.org/)
    * [Test a single asset from 14 locations](https://tools.keycdn.com/performance?url=https://territoire-numerique-libre.org) `*`
* HTTP/2
    * [Http2.pro](https://http2.pro/check?url=https://territoire-numerique-libre.org) `*` (check server HTTP/2, ALPN, and Server-push support)
* Global tools (webperf, accessibility, security, ...)
    * [Dareboost](https://www.dareboost.com)  (free trial)
    * [Sonarwhal](https://sonarwhal.com/scanner/)

* Social networks
  * [Twitter card validator](https://cards-dev.twitter.com/validator)
* structured data (JSON-LD, rdf, schema.org, microformats.org, ...)
  * [Google structured data testing tool](https://search.google.com/structured-data/testing-tool#url=https://territoire-numerique-libre.org/)  `*` 
  * [Yandex structured data testing tool](https://webmaster.yandex.com/tools/microtest/)
  * [Structured Data Linter](http://linter.structured-data.org/?url=https://territoire-numerique-libre.org)  `*` 
  * [Microdata Parser](http://tools.seomoves.org/microdata/)
* Google image
  * [images used on the website](https://www.google.fr/search?tbm=isch&q=site:territoire-numerique-libre.org)  `*`  (site:territoire-numerique-libre.org)
  * [images used on the website but hosted on other domains](https://www.google.fr/search?tbm=isch&q=site:territoire-numerique-libre.org+-src:territoire-numerique-libre.org) `*`  (site:territoire-numerique-libre.org -src:territoire-numerique-libre.org)
  * [images hosted on the domain name](https://www.google.fr/search?tbm=isch&q=src:territoire-numerique-libre.org)  `*`    (src:territoire-numerique-libre.org)
  * [images hosted on the domain name and used by other domain names (hotlinks)](https://www.google.fr/search?tbm=isch&q=src:territoire-numerique-libre.org+-site:territoire-numerique-libre.org)  `*`   (src:territoire-numerique-libre.org -site:territoire-numerique-libre.org)



