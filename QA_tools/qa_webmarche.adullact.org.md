# QA

https://webmarche.adullact.org

## URL
* [http  + with www](http://www.webmarche.adullact.org)
* [http  + without www](http://webmarche.adullact.org)
* [https + with www](https://www.webmarche.adullact.org)
* [https + without www](https://webmarche.adullact.org)

### Files

* [/humans.txt](https://webmarche.adullact.org/humans.txt)
* [/robots.txt](https://webmarche.adullact.org/robots.txt)
* [/.well-known/security.txt](https://webmarche.adullact.org/.well-known/security.txt)

## QA - Online tools
`*` preconfigured tools


* Security 
    * [Hardenize](https://www.hardenize.com) (DNS, SMTP, web server)
    * [Mozilla Observatory](https://observatory.mozilla.org/analyze/webmarche.adullact.org) `*` (HTTP header, SSL, cookies, ...)
    * [Security Headers](https://securityheaders.io/?q=https://webmarche.adullact.org) `*` (HTTP header)
    * Content-Security-Policy (CSP)
        * [cspvalidator.org](https://cspvalidator.org/#url=https://webmarche.adullact.org) `*` 
        * [csp-evaluator.withgoogle.com](https://csp-evaluator.withgoogle.com/?csp=https://webmarche.adullact.org) `*` 
    * SSL
        * [ssllabs.com](https://www.ssllabs.com/ssltest/analyze?d=webmarche.adullact.org) `*` 
        * [tls.imirhil.fr](https://tls.imirhil.fr/https/webmarche.adullact.org) `*` 
    * DNS
        * [DNSViz](http://dnsviz.net/d/webmarche.adullact.org/dnssec/) `*` (DNSSEC)
        * [DNSSEC Analyzer (Verisign Labs)](https://dnssec-debugger.verisignlabs.com/webmarche.adullact.org) `*`   (DNSSEC)
        * [Zonemaster (iiS and AFNIC)](https://zonemaster.net/domain_check)
* W3C tools
    * [HTML validator](https://validator.w3.org/nu/?doc=https://webmarche.adullact.org&showsource=yes&showoutline=yes&showimagereport=yes) `*` 
    * [CSS validator](https://jigsaw.w3.org/css-validator/validator?uri=https://webmarche.adullact.org&profile=css3) `*` 
    * [i18n checker](https://validator.w3.org/i18n-checker/check?uri=https://webmarche.adullact.org) `*` 
    * [Link checker](https://validator.w3.org/checklink?uri=https://webmarche.adullact.org&hide_type=all&depth=&check=Check) `*` 
* Web accessibility
    * [Asqatasun](https://app.asqatasun.org)
* Web perf
    * [Yellowlab](http://yellowlab.tools)
    * [Webpagetest](https://www.webpagetest.org/)
    * [Test a single asset from 14 locations](https://tools.keycdn.com/performance?url=https://webmarche.adullact.org) `*`
* HTTP/2
    * [Http2.pro](https://http2.pro/check?url=https://webmarche.adullact.org) `*` (check server HTTP/2, ALPN, and Server-push support)
* Global tools (webperf, accessibility, security, ...)
    * [Dareboost](https://www.dareboost.com)  (free trial)
    * [Sonarwhal](https://sonarwhal.com/scanner/)

* Social networks
  * [Twitter card validator](https://cards-dev.twitter.com/validator)
* structured data (JSON-LD, rdf, schema.org, microformats.org, ...)
  * [Google structured data testing tool](https://search.google.com/structured-data/testing-tool#url=https://webmarche.adullact.org/)  `*` 
  * [Yandex structured data testing tool](https://webmaster.yandex.com/tools/microtest/)
  * [Structured Data Linter](http://linter.structured-data.org/?url=https://webmarche.adullact.org)  `*` 
  * [Microdata Parser](http://tools.seomoves.org/microdata/)
* Google image
  * [images used on the website](https://www.google.fr/search?tbm=isch&q=site:webmarche.adullact.org)  `*`  (site:webmarche.adullact.org)
  * [images used on the website but hosted on other domains](https://www.google.fr/search?tbm=isch&q=site:webmarche.adullact.org+-src:webmarche.adullact.org) `*`  (site:webmarche.adullact.org -src:webmarche.adullact.org)
  * [images hosted on the domain name](https://www.google.fr/search?tbm=isch&q=src:webmarche.adullact.org)  `*`    (src:webmarche.adullact.org)
  * [images hosted on the domain name and used by other domain names (hotlinks)](https://www.google.fr/search?tbm=isch&q=src:webmarche.adullact.org+-site:webmarche.adullact.org)  `*`   (src:webmarche.adullact.org -site:webmarche.adullact.org)



