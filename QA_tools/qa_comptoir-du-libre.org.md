# QA

https://comptoir-du-libre.org

## URL
* [http  + with www](http://www.comptoir-du-libre.org)
* [http  + without www](http://comptoir-du-libre.org)
* [https + with www](https://www.comptoir-du-libre.org)
* [https + without www](https://comptoir-du-libre.org)

### Files

* [/humans.txt](https://comptoir-du-libre.org/humans.txt)
* [/robots.txt](https://comptoir-du-libre.org/robots.txt)
* [/.well-known/security.txt](https://comptoir-du-libre.org/.well-known/security.txt)

## QA - Online tools
`*` preconfigured tools


* Security 
    * [Hardenize](https://www.hardenize.com) (DNS, SMTP, web server)
    * [Mozilla Observatory](https://observatory.mozilla.org/analyze/comptoir-du-libre.org) `*` (HTTP header, SSL, cookies, ...)
    * [Security Headers](https://securityheaders.io/?q=https://comptoir-du-libre.org) `*` (HTTP header)
    * Content-Security-Policy (CSP)
        * [cspvalidator.org](https://cspvalidator.org/#url=https://comptoir-du-libre.org) `*` 
        * [csp-evaluator.withgoogle.com](https://csp-evaluator.withgoogle.com/?csp=https://comptoir-du-libre.org) `*` 
    * SSL
        * [ssllabs.com](https://www.ssllabs.com/ssltest/analyze?d=comptoir-du-libre.org) `*` 
        * [tls.imirhil.fr](https://tls.imirhil.fr/https/comptoir-du-libre.org) `*` 
    * DNS
        * [DNSViz](http://dnsviz.net/d/comptoir-du-libre.org/dnssec/) `*` (DNSSEC)
        * [DNSSEC Analyzer (Verisign Labs)](https://dnssec-debugger.verisignlabs.com/comptoir-du-libre.org) `*`   (DNSSEC)
        * [Zonemaster (iiS and AFNIC)](https://zonemaster.net/domain_check)
* W3C tools
    * [HTML validator](https://validator.w3.org/nu/?doc=https://comptoir-du-libre.org&showsource=yes&showoutline=yes&showimagereport=yes) `*` 
    * [CSS validator](https://jigsaw.w3.org/css-validator/validator?uri=https://comptoir-du-libre.org&profile=css3) `*` 
    * [i18n checker / fr](https://validator.w3.org/i18n-checker/check?uri=https://comptoir-du-libre.org/fr) `*` 
    * [Link checker](https://validator.w3.org/checklink?uri=https://comptoir-du-libre.org&hide_type=all&depth=&check=Check) `*` 
* Web accessibility
    * [Asqatasun](https://app.asqatasun.org)
* Web perf
    * [Yellowlab](http://yellowlab.tools)
    * [Webpagetest](https://www.webpagetest.org/)
    * [Test a single asset from 14 locations](https://tools.keycdn.com/performance?url=https://comptoir-du-libre.org) `*`
* HTTP/2
    * [Http2.pro](https://http2.pro/check?url=https://comptoir-du-libre.org) `*` (check server HTTP/2, ALPN, and Server-push support)
* Global tools (webperf, accessibility, security, ...)
    * [Dareboost](https://www.dareboost.com)  (free trial)
    * [Sonarwhal](https://sonarwhal.com/scanner/)

* Social networks
  * [Twitter card validator](https://cards-dev.twitter.com/validator)
* structured data (JSON-LD, rdf, schema.org, microformats.org, ...)
  * [Google structured data testing tool](https://search.google.com/structured-data/testing-tool#url=https://comptoir-du-libre.org/)  `*` 
  * [Yandex structured data testing tool](https://webmaster.yandex.com/tools/microtest/)
  * [Structured Data Linter](http://linter.structured-data.org/?url=https://comptoir-du-libre.org)  `*` 
  * [Microdata Parser](http://tools.seomoves.org/microdata/)
* Google image
  * [images used on the website](https://www.google.fr/search?tbm=isch&q=site:comptoir-du-libre.org)  `*`  (site:comptoir-du-libre.org)
  * [images used on the website but hosted on other domains](https://www.google.fr/search?tbm=isch&q=site:comptoir-du-libre.org+-src:comptoir-du-libre.org) `*`  (site:comptoir-du-libre.org -src:comptoir-du-libre.org)
  * [images hosted on the domain name](https://www.google.fr/search?tbm=isch&q=src:comptoir-du-libre.org)  `*`    (src:comptoir-du-libre.org)
  * [images hosted on the domain name and used by other domain names (hotlinks)](https://www.google.fr/search?tbm=isch&q=src:comptoir-du-libre.org+-site:comptoir-du-libre.org)  `*`   (src:comptoir-du-libre.org -site:comptoir-du-libre.org)



